


  var app = angular.module('newClue', ['ngTagsInput', 'ui.bootstrap']);
  app.controller(
  	'NewClueController',

  function($scope, $http) {
  	$scope.tagsValues = '';
  	

  	$scope.getCountry = function() {

  		$http({
  			method: 'GET',
  			url: 'http://localhost:8080/orion-orbit/newclue/country/list'
  		}).success(

  		function(data, status, headers,
  		config) {
  			$scope.getCountries = data;
  		}).error(

  		function(data, status, headers,
  		config) {

  		});
  	};
  	$scope.getCity = function() {
  		$scope.availableCities = [];

  		$http({
  			method: 'GET',
  			url: 'http://localhost:8080/orion-orbit/newclue/country/' + $scope.Countryselected.cntryCode + '/cities'
  		}).success(

  		function(data, status, headers,
  		config) {
  			$scope.getCities = data;
  		}).error(

  		function(data, status, headers,
  		config) {

  		});

  	};
  	$scope.contry = angular.copy.Countryselected;
  	$scope.city = angular.copy.cityselect;
  	$scope.getPlace = function() {
  		$scope.availablePlaces = [];
  		$http({
  			method: 'GET',
  			url: 'http://localhost:8080/orion-orbit/newclue/cities/ ' + $scope.cityselect.cityCode + '/clueAnswers'
  		}).success(

  		function(data, status, headers,
  		config) {

  			$scope.getPlaces = data;
  		}).error(

  		function(data, status, headers,
 		config) {

  		});

  		angular.forEach(
  		$scope.getPlaces,

  		function(value) {
  			if (value.getPlaces != ' ') {
  				$scope.availablePlaces.push(value);
  			}

  		});

  		sessionStorage.setItem('answers', JSON.stringify($scope.availablePlaces));
  		$scope.items = JSON.parse(sessionStorage.getItem('answers'));
        	 
  	};
               
  	$scope.loadTags = function($query) 
		
		

		 
		{
		    return $http.get('http://localhost:8080/orion-orbit/newclue/clueTag/list', { cache: true}).then(function(response) {
		      var countries = response.data;
		      return countries.filter(function(country) {
		        return country.tag.toLowerCase().indexOf($query.toLowerCase()) != -1;
		      });
		    });
		  };


	$scope.getTags = function() {
alert("hello");
		
		$scope.tagsValues = $scope.tags2.map(function(tag) {
			return tag.tagId;
		});
		

		
	};

  		$scope.submit = function(tags2) {

  			var formData = {
  				clue: $scope.selectedclue,
  				clueLvl: $scope.selected_rating,
  				clueDesc: $scope.cluedescription,
                ansId: $scope.placeselected.ansId,
  				tagIds: $scope.tagsValues 

  			};
  			alert("details are :-" + JSON.stringify(formData));
  			
  			var newcluepost = $http.post(
  				'http://localhost:8080/orion-orbit/newclue/saveAndSubmit',
  				JSON.stringify(formData));

  			newcluepost.success(function(data, status,
  			headers, config) {

  			});
  			newcluepost.error(function(data, status,
  			headers, config) {
  				alert("Exception details: " + JSON.stringify({
  					data: data
  				}));
  				
  			});

                $scope.Countryselected='';
                $scope.cityselect='';
                $scope.placeselected='';
                $scope.selectedclue='';
                $scope.selected_rating='';
                $scope.cluedescription='';
                $scope.tags2='';
  		};
  		
  	});