<!DOCTYPE html>
<html data-ng-app="myApp">

<script>document.write('<base href="' + document.location + '" />');</script>


<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"rel="Stylesheet"></link>


<!--   <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script> -->
  <script src="https://code.angularjs.org/1.2.19/angular.min.js"></script>
   <script src="http://cdn.jsdelivr.net/angular.bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="resources/js/ng-tags-input.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<link rel="stylesheet" href="resources/common/css/ng-myclue-tags.css"> 
 
  <link rel="stylesheet" href="resources/common/css/bootstrap.css" type="text/css">
  <link href="resources/common/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
  <link href="resources/common/css/difficulty level.css" rel="stylesheet" type="text/css">
    <script src="resources/js/bootstrap.min.js"></script>
  
  <script src="resources/js/myclue.js"></script>
 
        
        
<body  data-ng-app="myApp" data-ng-controller="myController">

<div class="container" id="modalmain">

<h3>MyClues</h3>

<div >


    
      <input type="text" class="form-control" placeholder="Search for..." data-ng-model="searchText">
      
 

<table class="table table-hover" data-ng-init="getData()">
<thead><tr>
   
    <th>Clue</th>
    <th>Answer</th>
     <th>Action</th>
    
  </tr></thead>
  <tbody>
  <tr data-ng-repeat="x in getDatas | filter:searchText" data-ng-model="selectedid">
  
   <td>{{ x.clueData.clue }}</td>
    <td>{{ x.clueAns.ans }}</td>
   
  
    <td>
    
      
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" id="button-{{ x.clue_Ans_ID }}" data-ng-click="getId(x.clue_Ans_ID); getTagMap();">
      <span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Edit
      </button>
<!--   <a href="newclue/clue/{{x.clue_Ans_ID}}/ans"> -->
<!--           <button class="btn btn-default"  >Edit</button> -->
<!--           </a> -->
      <button class="btn btn-danger" data-toggle="modal" data-target="#myModal1" data-ng-click="getId(x.clue_Ans_ID)" >delete</button>
      
      
      
      
      
    </td>
    
  </tr></tbody>

</table>

</div>

<hr>


</div>

<div id="myModal1" class="modal fade" role="dialog" >
  <div class="modal-dialog"style="width: 700px; height: 500px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Orion Orbit</h4>
      </div>
      <div class="modal-body"  >
        <center>
        <h2><font color="blue">Want to delete clue ..</font></h2>
        </center>
        <div data-ng-init="getId()">
             <button class="btn btn-danger"  data-ng-click="removeC(getClueAns.clue_Ans_ID )" id="button-{{ getClueAns.clue_Ans_ID }}">delete</button>  
             
      </div>
        </div></div>
      </div ></div>


<div id="myModal" class="modal fade" role="dialog" data-ng-init="getId(rowId)" >
  <div class="modal-dialog"style="width: 700px; height: 500px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Orion Orbit</h4>
      </div>
      <div class="modal-body"  >
        <center>
        <h2><font color="blue">Edit Your Clue Here ..</font></h2></center>
      </div >
      <form name="form" class="css-form" novalidate >
      <div >
          <div >
          <div data-ng-init="getId()" >
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Country:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" ng-readonly="true" data-ng-model="getClueAns.clueAns.cntryCode.cntryName"   style="border: 2px solid grey; border-radius: 5px; width: 273px; height: 45px" />
              </div>
                <br>
                <div data-ng-init="getId()" >
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>City:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" ng-readonly="true" data-ng-model="getClueAns.clueAns.cityCode.cityName"   style="border: 2px solid grey; border-radius: 5px; width: 273px; height: 45px" />
              </div>
                <br>
               <br>
              <div class="" id="faith_voting" data-ng-init="getId()" data-ng-click="getTags()">
        <fieldset class="rating" id="rating_1" ><b>Difficulty:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
                  <input data-ng-model="getClueAns.clueData.clueLvl"type="radio" id="star7" name="rating" value="7" />
                  <label data-ng-model="getClueAns.clueData.clueLvl"for="star7" title="LEVEL 7">7 stars</label>
                  <input data-ng-model="getClueAns.clueData.clueLvl"type="radio" id="star6" name="rating" value="6" />
                  <label data-ng-model="getClueAns.clueData.clueLvl"for="star6" title="LEVEL 6">6 stars</label>
                  <input data-ng-model="getClueAns.clueData.clueLvl"type="radio" id="star5" name="rating" value="5" />
                  <label data-ng-model="getClueAns.clueData.clueLvl"for="star5" title="LEVEL 5">5 stars</label>
                  <input data-ng-model="getClueAns.clueData.clueLvl"type="radio" id="star4" name="rating" value="4" />
                  <label data-ng-model="getClueAns.clueData.clueLvl"for="star4" title="LEVEL 4">4 stars</label>
                  <input data-ng-model="getClueAns.clueData.clueLvl"type="radio" id="star3" name="rating" value="3" />
                  <label data-ng-model="getClueAns.clueData.clueLvl"for="star3" title="LEVEL 3">3 stars</label>
                  <input data-ng-model="getClueAns.clueData.clueLvl" type="radio" id="star2" name="rating" value="2" />
                  <label data-ng-model="getClueAns.clueData.clueLvl"for="star2" title="LEVEL 2">2 stars</label>
                  <input data-ng-model="getClueAns.clueData.clueLvl"type="radio" id="star1" name="rating" value="1" />
                  <label data-ng-model="getClueAns.clueData.clueLvl"for="star1" title="LEVEL 1">1 star</label>
                  <span ng-show="!getClueAns.clueData.clueLvl">Set the Diff level for Clue!!</span>
                  </fieldset>
              </div>
                <br>
              <br>
              <div data-ng-init="getId()">
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Answer/place:</b>&nbsp;&nbsp; <input type="text" ng-readonly="true"  data-ng-model="getClueAns.clueAns.ans"   style="border: 2px solid grey; border-radius: 5px; width: 273px; height: 54px" />
              </div>
              
              
              <br>
              <div data-ng-init="getId()" data-ng-click="getTags()">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Clue:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <textarea data-ng-model=" getClueAns.clueData.clue"   cols="4" id="Clue"   style="border: 2px solid grey; border-radius: 5px; width: 273px; height: 54px" /></textarea>
              <span ng-show="!getClueAns.clueData.clue"> Clue cannot be Empty!!</span>
              </div>
              
              <br>
              <div data-ng-init="getId()"data-ng-click="getTags()" >
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><middle>Clue Desc :</middle>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><textarea data-ng-model="getClueAns.clueData.clueDesc" cols="4"  style="border: 2px solid grey; border-radius: 5px; width: 271px; height: 62px" /></textarea>
               <span ng-show="!getClueAns.clueData.clueDesc"> Clue Desc cannot be empty!!</span>
              </div>
              
              
           <br>
              <div>
              <label for="tags2"></label><b><fontsize="4" face="footlight mt light"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tags :</b>
                </font>
                </b>
               <tags-input   data-ng-model="newTags" display-property="tag" 
              add-on-enter="true"
              ng-disable="true"
              min-tags="1"
                template="tag-template" on-tag-added="getTags();addTags();" on-tag-removed="removedTagId($index);getTags();">
      <auto-complete source="loadTags($query)"
                     min-length="2"
                     load-on-focus="true"
                     load-on-empty="true"
                     max-results-to-show="32"
                     template="autocomplete-template"></auto-complete>
    </tags-input>
    <span ng-show="!newTags">Tags Cannot be empty!!</span>
    </div>
   <script type="text/ng-template" id="tag-template"> 
        <div class="tag-template">
        <div class="left-panel">
        
        </div>
        <div class="right-panel">
          <span>{{$getDisplayText()}}</span>
          <a class="remove-button" ng-click="$removeTag()">&#10006;</a>
        </div>
      </div>
    </script>
    
   <script type="text/ng-template" id="autocomplete-template">
      <div class="autocomplete-template">
        <div class="left-panel">
        
        </div>
        <div class="right-panel">
          <span ng-bind-html="$highlight($getDisplayText())"></span>
          
        </div>
      </div>
    </script>
      <br>
      <br>
      
          <center>
          
          
          <button type="submit"  class="btn btn-info" data-dismiss="modal" ng-disabled=" (getClueAns.clueData.clueLvl && getClueAns.clueData.clue && getClueAns.clueData.clueDesc && newTags ) ?  false : true"   data-ng-click="updateClue()" >Save</button>
          
           &nbsp;&nbsp;&nbsp;&nbsp;
 <a href="index.jsp">
 <button class="btn btn-default"  data-dismiss="modal" >Cancel</button>
 </a>
 
 </center>
      <br>
      <br>
      

  </div>
  </div>
  </form>
</div>
</div>
</div>



</body>
</html>