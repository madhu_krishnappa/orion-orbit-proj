package com.orion.orbit.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.orion.orbit.controller.ClueController;
import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.ClueTag;
import com.orion.orbit.model.ClueTagMap;
import com.orion.orbit.model.CountryCode;
import com.orion.orbit.model.NewClueRequestVO;

public class ClueDaoImpl implements ClueDao {
	
	static final Logger logger = Logger.getLogger(ClueDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;


	@Override
	public AnsClueMap getAnsClue(long id) throws Exception {
		session = sessionFactory.openSession();
		AnsClueMap ansClueMap = (AnsClueMap) session.load(AnsClueMap.class,new Long(id));
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return ansClueMap;
	}

	@Override
	public List<AnsClueMap> getAnsClue() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<AnsClueMap> ansClueList = session.createCriteria(AnsClueMap.class).list();
		tx.commit();
		session.close();
		return ansClueList;
	}

	@Override
	public List<CountryCode> getCountry() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<CountryCode> countryCodeList = session.createCriteria(CountryCode.class)
				.list();
		tx.commit();
		session.close();
		return countryCodeList;
	}

	@Override
	public List<CityCode> getCities(long id) throws Exception {
		session = sessionFactory.openSession();
		CountryCode countryCode = (CountryCode) session
				.createCriteria(CountryCode.class)
				.add(Restrictions.eq("cntryCode", id)).uniqueResult();
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return countryCode.getCityCode();
	}

	@Override
	public List<ClueTagMap> getTagMap(long id) throws Exception {
		session = sessionFactory.openSession();
		ClueData clueId = (ClueData) session
				.createCriteria(ClueData.class)
				.add(Restrictions.eq("clueId", id)).uniqueResult();
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return clueId.getClueMap();
	}


	@Override
	public List<ClueAns> getClueAnswers(long id) throws Exception {
		session = sessionFactory.openSession();
		CityCode cityCode = (CityCode) session
				.createCriteria(CityCode.class)
				.add(Restrictions.eq("cityCode", id)).uniqueResult();
		tx = session.getTransaction();
		session.beginTransaction();
		
		tx.commit();
		return cityCode.getClueAnswer();
	}
	
	@Transactional
	public List<ClueTag> getClueTagList() throws Exception {
		System.out.println("Inside getClueTagList in dao impl");
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<ClueTag> clueTagList = session
				.createCriteria(ClueTag.class).list();
		tx.commit();
		session.close();
		return clueTagList;
	}
	
	@Transactional
	public void save(AnsClueMap ansClueMap) throws Exception
	{
		System.out.println("Inside dao impl");
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(ansClueMap);
		tx.commit();		
		session.close();
		
	}
	
	@Transactional
	public void save(ClueTagMap clueTagMapping) throws Exception
	{
		System.out.println("Inside dao impl");
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(clueTagMapping);
		tx.commit();
		session.close();
		
	}

	@Transactional
	public void save(NewClueRequestVO newClueRequestVO) throws Exception 
	{
		System.out.println("Inside dao impl");
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(newClueRequestVO);	
		tx.commit();
		session.close();
	}

	@Transactional
	public void save(ClueData clueData) throws Exception 
	{
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(clueData);	
		tx.commit();
		session.close();
		
	}
	
	@Override
	public void update(ClueData clue) throws Exception {
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.update(clue);	
		tx.commit();
		session.close();
		   }

	@Override
	public void save(ClueAns clueAns) throws Exception {
		

		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(clueAns);	
		tx.commit();
		session.close();
	}

	
	
	 
	@Override
	public ClueData getByClueData_ID(long clueId) throws Exception {
		return (ClueData) sessionFactory.getCurrentSession().get(ClueData.class, clueId);
	}

	
	@Override
	public void deleteClue(long id) throws Exception {
		session = sessionFactory.openSession();
		Object del = session.load(AnsClueMap.class, id);
		tx = session.getTransaction();
		session.beginTransaction();
		session.delete(del);
		tx.commit();
		session.close();
	}
	
	@Override
	public void deleteTag(ClueTag tag) throws Exception {
		session = sessionFactory.openSession();
	    tx = session.getTransaction();
		session.beginTransaction();
		String sql = "DELETE FROM com.orion.orbit.model.ClueTag WHERE tags=:tag";
		Query query = session.createQuery(sql);
		query.setParameter("tag", tag);
		int result = query.executeUpdate();
       System.out.println("Numer of records effected due to delete query"+result);
		tx.commit();
		session.close();
		
	}
	
	@Transactional
	public void deleteTagMap(ClueData id) throws Exception {
		session = sessionFactory.openSession();
	    tx = session.getTransaction();
		session.beginTransaction();
		String sql = "DELETE FROM com.orion.orbit.model.ClueTagMap WHERE clueData=:ID";
		Query query = session.createQuery(sql);
		query.setParameter("ID", id);
		int result = query.executeUpdate();
       System.out.println("Numer of records effected due to delete query"+result);

		
		
		tx.commit();
		session.close();
		
	}
	
	




	@Override
	public void updateTag(ClueTagMap tag) throws Exception {
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		System.out.println("Inside update tag2 service impl");

		session.update(tag);	
		tx.commit();
		session.close();
		
	}

	@Override
	public void save(ClueTag tag) throws Exception {
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(tag);
		tx.commit();		
		session.close();
		
	}

	@Override
	public void getTag(ClueTag Gtag) throws Exception {
		session = sessionFactory.openSession();
	    tx = session.getTransaction();
		session.beginTransaction();
		String sql = "SELECT * FROM com.orion.orbit.model.ClueTag WHERE tags=:Gtag";
		Query query = session.createQuery(sql);
		query.setParameter("tag", Gtag);
		int result = query.executeUpdate();
       System.out.println("Numer of records effected due to delete query"+result);
		tx.commit();
		session.close();
		
	}


}
