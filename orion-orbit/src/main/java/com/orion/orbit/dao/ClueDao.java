package com.orion.orbit.dao;

import java.util.List;

import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.ClueTag;
import com.orion.orbit.model.ClueTagMap;
import com.orion.orbit.model.CountryCode;
import com.orion.orbit.model.NewClueRequestVO;

public interface ClueDao {

	public List<CountryCode> getCountry() throws Exception;
	public List<CityCode> getCities(long id) throws Exception;
	public List<ClueAns> getClueAnswers(long id) throws Exception;
	public void save(ClueData clueData)throws Exception;
	public void save(ClueAns clueAns)throws Exception;
	public void update(ClueData clue)throws Exception;
	public void updateTag(ClueTagMap tag)throws Exception;
	
    public List<ClueTagMap> getTagMap(long id)throws Exception;

	
	public AnsClueMap getAnsClue(long id) throws Exception;
	public List<AnsClueMap> getAnsClue() throws Exception;
	public List<ClueTag> getClueTagList() throws Exception;
	public void save(ClueTagMap clueTagMapping)throws Exception;
	public void save(AnsClueMap ansClueMap)throws Exception;
	public void save(NewClueRequestVO newClueRequestVO) throws Exception;
		
	public void deleteClue(long id) throws Exception;
	public void deleteTag(ClueTag tag)throws Exception;
    public void deleteTagMap(ClueData id) throws Exception;
	public void save(ClueTag tag)throws Exception;
    
    public void getTag(ClueTag Gtag)throws Exception;

    
	public ClueData getByClueData_ID(long clueId)throws Exception ;
		

}
















































































































































































































































































