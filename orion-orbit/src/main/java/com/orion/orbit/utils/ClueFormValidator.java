package com.orion.orbit.utils;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.orion.orbit.model.UpdateClue;


@Component("clueFormValidator")
public class ClueFormValidator implements Validator
{

	@SuppressWarnings("unchecked")
	@Override
	public boolean supports(Class<?> clazz)
	{
		return UpdateClue.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object model, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "clue","required.clue", "Clue is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "clueDesc","required.clueDesc", "Clue Desc is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "clueLvl","required.clueLvl", "Clue Level is required.");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "QUESTION","required.QUESTION", "can not be blank.");
		
		/*ValidationUtils.rejectIfEmpty(errors, "START_DATE","required.SURVEY_NAME", "Enter date");*/
	}

}

