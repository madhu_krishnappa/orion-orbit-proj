package com.orion.orbit.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.orion.orbit.dao.ClueDao;
import com.orion.orbit.exception.ClueNotFoundException;
import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.ClueTag;
import com.orion.orbit.model.ClueTagMap;
import com.orion.orbit.model.CountryCode;
import com.orion.orbit.model.NewClueRequestVO;
import com.orion.orbit.model.UpdateClue;
import com.orion.orbit.model.User;
import com.orion.orbit.services.ClueServices;
import com.orion.orbit.utils.ClueFormValidator;


@Controller
@RequestMapping("/newclue")
public class ClueController {

	@Autowired
	ClueServices clueServices;
	
	@Autowired
	ClueDao clueDAO;
	
	@Autowired
	ClueFormValidator validator;
	
	static final Logger logger = Logger.getLogger(ClueController.class);
	
	@ExceptionHandler(ClueNotFoundException.class)
	public ModelAndView handleException(HttpServletRequest request, Exception ex){
		logger.error("Requested URL="+request.getRequestURL());
		logger.error("Exception Raised="+ex);
		
		ModelAndView modelAndView = new ModelAndView();
	    modelAndView.addObject("exception", ex);
	    modelAndView.addObject("url", request.getRequestURL());
	    
	    modelAndView.setViewName("error");
	    
	    return modelAndView;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public String viewLogin(Map<String, Object> model) {
        User user = new User();
        model.put("userForm", user);
        return "LoginForm";
    }
 
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(@Valid @ModelAttribute("userForm") User userForm,
            BindingResult result, Map<String, Object> model) {
 
        if (result.hasErrors()) {
            return "LoginForm";
        }
 
        return "LoginSuccess";
    }
	
	
	@RequestMapping(value = "clue/{id}/ans", method = RequestMethod.GET)
	public @ResponseBody
	AnsClueMap getAnsClue(@PathVariable("id") long id) throws Exception {
		AnsClueMap ansClueMap = null; 
		try {
			ansClueMap= clueServices.getAnsClue(id);

		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}
		return ansClueMap;
	}
	
//	@RequestMapping(value="clue/{id}/ans", method=RequestMethod.GET)
//	public ModelAndView edit(@PathVariable("id")long id) throws Exception
//	{
//		ModelAndView mav = new ModelAndView("editClue");
//		AnsClueMap ansclue = clueDAO.getAnsClue(id);
//		mav.addObject("editClue", ansclue);
//		return mav;
//	}
	
	@RequestMapping(value = "clueAns/list", method = RequestMethod.GET)
	public @ResponseBody
	List<AnsClueMap> getAnsClue() throws Exception {

		List<AnsClueMap> ansClueList = null;
		try {
			ansClueList= clueServices.getAnsClue();
		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}

		return ansClueList;
	}
	
	@RequestMapping(value = "country/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CountryCode> getCountrycode() throws Exception{

		List<CountryCode> countryCodeList = null;
		try {
			countryCodeList = clueServices.getCountry();
		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}

		return countryCodeList;
	}
	
	@RequestMapping(value = "country/{id}/cities", method = RequestMethod.GET)
	public @ResponseBody
	List<CityCode> getCitiesByCountryCode(@PathVariable("id") long id) throws Exception{
		List<CityCode> cities = null;
		try {
			cities = clueServices.getCities(id);
		}catch (Exception e) {
			throw new ClueNotFoundException(e);
		}
		return cities;
	}
	
	@RequestMapping(value = "tag/{id}/map", method = RequestMethod.GET)
	public @ResponseBody
	List<ClueTagMap> getTagMap(@PathVariable("id") long id) throws Exception {
		List<ClueTagMap> tagMap = null; 
		try {
			tagMap= clueServices.getTagMap(id);
			
			

		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}
		return tagMap;
	}
	
	@RequestMapping(value = "cities/{id}/clueAnswers", method = RequestMethod.GET)
	public @ResponseBody
	List<ClueAns> getClueanswerByCityCode(@PathVariable("id") long id) throws Exception{
		List<ClueAns> clueAnswers = null;
		try {
			clueAnswers = clueServices.getClueAnswers(id);
		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}
		return clueAnswers;
	}
	
	
@RequestMapping(value = "/saveAndSubmit", method = RequestMethod.POST)
	
	public @ResponseBody void save(@RequestBody NewClueRequestVO newClueRequestVO )throws Exception

	{	
		System.out.println(" Inside saveAndSubmit method ");	
		System.out.println("clue:-" +newClueRequestVO.getClue() );
		System.out.println("level:-" +newClueRequestVO.getClueLvl() );	
		System.out.println("clueDESC:-" +newClueRequestVO.getClueDesc());	
		System.out.println("clueAnsId:-"+newClueRequestVO.getAnsId());		
		System.out.println("clueTag size:-"+newClueRequestVO.getTagIds().length);
		ClueData clueData = new ClueData();
		clueData.setClue(newClueRequestVO.getClue());
		clueData.setClueLvl(newClueRequestVO.getClueLvl());
		clueData.setClueDesc(newClueRequestVO.getClueDesc());
		ClueAns clueAns=new ClueAns();
		clueAns.setAnsId(newClueRequestVO.getAnsId());
		
		newClueRequestVO.setClueValidationRuleRuleId(5);
		clueData.setClueValidationRuleRuleId(5);
		try {
			    
				clueServices.save(clueData);
				System.out.println(" Inside");		
				AnsClueMap ansClueMap=new AnsClueMap();
				ansClueMap.setClueData(clueData);
				ansClueMap.setClueAns(clueAns);
				clueServices.save(ansClueMap);
				for (int i = 0; i < newClueRequestVO.getTagIds().length; i++) {
					ClueTagMap clueTagMap=new ClueTagMap();
					ClueTag clueTag=new ClueTag();
					System.out.println("tagid---->"+newClueRequestVO.getTagIds()[i]);	
					clueTag.setTagId(newClueRequestVO.getTagIds()[i]);	
					clueTagMap.setClueData(clueData);		
					clueTagMap.setClueTag(clueTag);				
					clueServices.save(clueTagMap);		
				}						
				System.out.println(" Outside");	
			} catch (Exception e) {
	 				throw new ClueNotFoundException(e);
	 			}
	 }




	
@RequestMapping(value = "/updateTag/Submit", method = RequestMethod.PUT)
public @ResponseBody void updateTag(@RequestBody UpdateClue updateClue )throws Exception 
{	
	System.out.println("Inside update Tag controller method");
	
//	validator.validate(updateClue, result);
//    
//    if (result.hasErrors()) {
//        return "myclue";
//    }
   
	ClueData clue = new ClueData();
	clue.setId(updateClue.getClueId());
	clue.setClue(updateClue.getClue());
	clue.setClueDesc(updateClue.getClueDesc());
	clue.setClueLvl(updateClue.getClueLvl());
	
	ClueData cluet = new ClueData();
	cluet.setId(updateClue.getClueId());

try {
	clueServices.update(clue);
	System.out.println(" Inside cont tag");
	System.out.println(" clueid:" +updateClue.getClueId());
	
	clueServices.deleteTagMap(cluet);
	
	for (int i = 0; i < updateClue.getTagsPush().length; i++) {
		ClueTag newTag=new ClueTag();
		ClueTagMap clueTagMap = new ClueTagMap();
    	newTag.setTag(updateClue.getTagsPush()[i]);
		clueServices.save(newTag);
		clueTagMap.setClueData(clue);
		clueTagMap.setClueTag(newTag);
		clueServices.save(clueTagMap);
	}	
		
	
	
	
	
	for (int i = 0; i < updateClue.getTagIds().length; i++) {
		ClueTagMap clueTagMap = new ClueTagMap();
		ClueTag clueTag=new ClueTag();
		
		System.out.println("tagid---->"+updateClue.getTagIds()[i]);	                 
		clueTag.setTagId(updateClue.getTagIds()[i]);	               
		clueTagMap.setClueData(clue);
		clueTagMap.setClueTag(clueTag);
		clueServices.save(clueTagMap);
		}
	    
  }
	
          catch (Exception e) {
			
        	  e.printStackTrace();
			
             }



}


		
		
	
	
//@RequestMapping(value = "/update/Submit", method = RequestMethod.PUT)
//		public @ResponseBody void updateClue(@RequestBody ClueData clue)throws Exception
//	{	
//		System.out.println(" Inside update And Submit controller method ");
//		System.out.println("clueid:-" +clue.getClueId());
//	System.out.println("clue:-" +clue.getClue());
//	System.out.println("level:-" +clue.getClueLvl() );	
//	System.out.println("clueDESC:-" +clue.getClueDesc());
//	
//	
//       	
//	try {
//		clueServices.update(clue);
//       
//			
//							} 
//	                 catch (Exception e) {
//	 				 throw new ClueNotFoundException(e);
//	 			}
//		
//	 }


	
	
	
	
    @RequestMapping(value = "clueTag/list", method = RequestMethod.GET)
	 public @ResponseBody
	 List<ClueTag> getCluetag() throws Exception{
	 	
     System.out.println(" *** Inside getCluetag method in ClueController ***");
	 List<ClueTag> clueTagList = null;
	 	try {	 		
	 	clueTagList = clueServices.getClueTagList();
	 	System.out.println("Size---->"+clueTagList.size());
	 	}catch (Exception e) {
 		throw new ClueNotFoundException(e);
 		}
	 return clueTagList;
 	}
	 

	 
    @RequestMapping(value = "delete/{id}/Clue", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id") long id  ) throws Exception {
    

		try {
			clueServices.deleteClue(id);
			
		} catch (Exception e) {
			throw new ClueNotFoundException(e);
		}
			

    }
    
    
}
	




	


