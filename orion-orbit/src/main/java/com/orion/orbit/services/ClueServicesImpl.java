package com.orion.orbit.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.orion.orbit.dao.ClueDao;
import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.ClueTag;
import com.orion.orbit.model.ClueTagMap;
import com.orion.orbit.model.CountryCode;
import com.orion.orbit.model.NewClueRequestVO;



public class ClueServicesImpl implements ClueServices {

	@Autowired
	ClueDao clueDao;


	@Override
	public AnsClueMap getAnsClue(long id) throws Exception {
		return clueDao.getAnsClue(id);
	}

	@Override
	public List<AnsClueMap> getAnsClue() throws Exception {
		return clueDao.getAnsClue();
	}

	@Override
	public List<CountryCode> getCountry() throws Exception {
		return clueDao.getCountry();
	}

   @Override
	public List<CityCode> getCities(long id) throws Exception {
		return clueDao.getCities(id);
	}

	@Override
    public List<ClueAns> getClueAnswers(long id) throws Exception {
		return clueDao.getClueAnswers(id);

	}

	
	public void save(ClueData clueData) throws Exception
			//,List<ClueTag> clueTagList) throws Exception
	{
		System.out.println("Inside data service impl");
		clueDao.save(clueData);
		
				//,clueTagList);
		
	}
	
	@Override
	public void save(ClueAns clueAns) throws Exception {
		System.out.println("Inside ans service impl");
		clueDao.save(clueAns);
		
	}
	
	@Override
	public void update(ClueData clue) throws Exception {
		System.out.println("Inside update data service impl");
		clueDao.update(clue);
	}
	
	public void save(AnsClueMap ansClueMap) throws Exception {
		clueDao.save(ansClueMap);
	}
	
	public void save(ClueTagMap clueTagMapping) throws Exception {
		clueDao.save(clueTagMapping);
	}
	
	
	public List<ClueTag> getClueTagList() throws Exception {
		
		System.out.println("***Inside getClueTagList in data service impl***");
		return clueDao.getClueTagList();
	}


public void save(NewClueRequestVO newClueRequestVO) throws Exception {
	
	System.out.println("Inside data service impl");
	clueDao.save(newClueRequestVO);
	
}



@Override
public ClueData getByClueData_ID(long clueId) throws Exception {
	return clueDao.getByClueData_ID(clueId);
}

@Override
public void deleteClue(long id) throws Exception {
	clueDao.deleteClue(id);
	
}

@Override
public void deleteTagMap(ClueData id) throws Exception {
	clueDao.deleteTagMap(id);
	
}

@Override
public void deleteTag(ClueTag tag) throws Exception {
	clueDao.deleteTag(tag);
	
}




@Override
public void updateTag(ClueTagMap tag) throws Exception {
	System.out.println("Inside update tag service impl");
	clueDao.updateTag(tag);
	
}

@Override
public List<ClueTagMap> getTagMap(long id) throws Exception {
	return clueDao.getTagMap(id);

}

@Override
public void save(ClueTag tag) throws Exception {
	clueDao.save(tag);
	
}

@Override
public void getTag(ClueTag Gtag) throws Exception {
	clueDao.getTag(Gtag);
	
}




}
