package com.orion.orbit.anno;

import javax.validation.ConstraintValidatorContext;

public interface ValidatorDao {
	 public void initialize(Madhu value);
	 
	 public boolean isValid(int valueField, ConstraintValidatorContext cxt);
	 
	 

}
