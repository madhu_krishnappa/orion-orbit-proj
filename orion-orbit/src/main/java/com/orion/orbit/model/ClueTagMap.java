package com.orion.orbit.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "clue_tag_mapping")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class ClueTagMap implements Serializable{

	private static final long serialVersionUID = 1L;
	
    @Id
	@ManyToOne
	@JoinColumn(name="CLUE_DATA_Clue_ID", nullable=false)
	private ClueData clueData;
	
	@Id
	@ManyToOne        
	@JoinColumn(name="CLUE_TAGS_Tag_ID", nullable=false)
	private ClueTag clueTag;
	
	public ClueTagMap() {		
	}
	
	public ClueTagMap(ClueData clueData, ClueTag clueTag) {
		super();
		this.clueData = clueData;
		this.clueTag = clueTag;
	}

	public ClueTag getClueTag() {
		return clueTag;
	}

	public  void setClueTag(ClueTag clueTag) {
		this.clueTag = clueTag;
	}

	public ClueData getClueData() {
		return clueData;
	}

	public void setClueData(ClueData clueData) {
		this.clueData = clueData;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}


