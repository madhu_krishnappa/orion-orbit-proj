package com.orion.orbit.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Transient;


public class NewClueRequestVO {
	
	
	private long ansId;
	private String ans;
	private String clue;	
	private String clueDesc;
	private int clueLvl;
	private long clueValidationRuleRuleId;
	private long[] tagIds;
	private String tag;
	
	
	
	
		
	

	public long[] getTagIds() {
		return tagIds;
	}

	public void setTagIds(long[] tagIds) {
		this.tagIds = tagIds;
	}
	
	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}


	public long getAnsId() {
		return ansId;
	}

	public void setAnsId(long ansId) {
		this.ansId = ansId;
	}

	public String getAns() {
		return ans;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public String getClue() {
		return clue;
	}

	public void setClue(String clue) {
		this.clue = clue;
	}

	public String getClueDesc() {
		return clueDesc;
	}

	public void setClueDesc(String clueDesc) {
		this.clueDesc = clueDesc;
	}

	public int getClueLvl() {
		return clueLvl;
	}

	public void setClueLvl(int clueLvl) {
		this.clueLvl = clueLvl;
	}

	public long getClueValidationRuleRuleId() {
		return clueValidationRuleRuleId;
	}

	public void setClueValidationRuleRuleId(long clueValidationRuleRuleId) {
		this.clueValidationRuleRuleId = clueValidationRuleRuleId;
	}
}
