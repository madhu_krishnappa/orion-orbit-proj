package com.orion.orbit.model;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@Entity
@Table(name = "clue_tags",uniqueConstraints={@UniqueConstraint(columnNames={"tag_Id"})})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClueTag implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "tag_Id", nullable=false, unique=true, length=11)
	private long tagId;

	@Column(name = "tags")
	private String tags;
	

	public String getTag() {
		return tags;
	}

	public void setTag(String tag) {
		this.tags = tag;
	}

	public long getTagId() {
		return tagId;
	}

	public void setTagId(long tagId) {
		this.tagId = tagId;
	}

	public static Iterator iterator() {
			return null;
	}
	
}


