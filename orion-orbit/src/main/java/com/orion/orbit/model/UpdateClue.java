package com.orion.orbit.model;

import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;

public class UpdateClue {
	
	@Id
	private long clueId;
	
	
	private String clue;
	private String clueDesc;
	private int clueLvl;
	private long[] tagIds;
	private String[] tagsPush;
	
	
	
	
	
	
	
	public String[] getTagsPush() {
		return tagsPush;
	}
	public void setTagsPush(String[] tagsPush) {
		this.tagsPush = tagsPush;
	}
	public long getClueId() {
		return clueId;
	}
	public void setClueId(long clueId) {
		this.clueId = clueId;
	}
	public String getClue() {
		return clue;
	}
	public void setClue(String clue) {
		this.clue = clue;
	}
	public String getClueDesc() {
		return clueDesc;
	}
	public void setClueDesc(String clueDesc) {
		this.clueDesc = clueDesc;
	}
	public int getClueLvl() {
		return clueLvl;
	}
	public void setClueLvl(int clueLvl) {
		this.clueLvl = clueLvl;
	}
	public long[] getTagIds() {
		return tagIds;
	}
	public void setTagIds(long[] tagIds) {
		this.tagIds = tagIds;
	}
	
	

}
